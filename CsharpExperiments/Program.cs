﻿using System;

namespace DIContainer
{
    class Program
    {
        static void Main(string[] args)
        {
            DIContainer container = new DIContainer();
            container.Register<ICar, Car>();
            container.Register<IEngine, GoodEngine>();


            ICar car = container.Resolve<ICar>();
            car.Start();
        }
        public interface ICar
        {
            void Start();
        }
        public class Car:ICar
        {
            private readonly IEngine engine;
            public Car(IEngine engine)
            {
                this.engine = engine;

            }
            public void Start()
            {
                var startMessage = engine.StartMessage();
                Console.WriteLine(startMessage);
            }

        }
        public interface IEngine
        {
            string StartMessage();
        }
        public class GoodEngine : IEngine
        {
            public string StartMessage()
            {
                return "wrrrum wrrrumm";
            }
        }

        public class BadEngine : IEngine
        {
            public string StartMessage()
            {
                return "Nigdzie sie nie ruszam.";
            }
        }

    }
}
   