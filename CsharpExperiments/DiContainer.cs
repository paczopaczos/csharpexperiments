﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DIContainer
{
    public  class DIContainer
    {
         private Dictionary<Type, Type> registeredTypes= new Dictionary<Type, Type>();

        public  void Register<Tkey, Tval>()
        {
            Type tkey = typeof(Tkey);
            Type tval = typeof(Tval);
            if (!registeredTypes.ContainsKey(tkey))
            {
                registeredTypes.Add(tkey, tval);
            }
            else
            {
                throw new Exception("Typ "+tkey.Name+" został zarejestrowany wcześniej.");
            }
        }
        public   Tkey  Resolve<Tkey>()
        {
            return (Tkey) Resolve(typeof(Tkey));
        }
        private  object Resolve(Type Tkey)
        {
          
            if (registeredTypes.ContainsKey(Tkey))
            {
                Type tval = registeredTypes[Tkey];
                
                var firstConstructor = tval.GetConstructors().First();
                var constructorParams = firstConstructor.GetParameters();
                if (constructorParams.Count() == 0)
                {
                    
                    return  Activator.CreateInstance(tval);
                }
                else
                {
                    var resolvedConstructorParams = new List<object>();
                    foreach (var param in constructorParams)
                    {

                        object resParam = Resolve(param.ParameterType);
                        resolvedConstructorParams.Add(resParam);
                    }
                    return firstConstructor.Invoke(resolvedConstructorParams.ToArray());

                }


            }
            else
            {
                throw new Exception("Typ" + Tkey.Name + "nie został zarejestrowany.");
            }
        }
    }
}
