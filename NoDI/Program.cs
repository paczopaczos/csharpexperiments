﻿using System;

namespace NoDI
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
        }
    }
    class Car
    {
        private readonly IEngine engine;
        public Car()
        {
            engine = new GoodEngine();
        }
        public void Start()
        {
            var startMessage=engine.StartMessage();
            Console.WriteLine(startMessage);
        }
    }
    public interface IEngine
    {
        string StartMessage();

    }
    class GoodEngine : IEngine
    {
        public string StartMessage()
        {
            return "wrrrum wrrrumm";

        }
    }
}
