﻿using System;

namespace InterfaceDI
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
            IEngine goodEngine = new GoodEngine();
            car1.Inject(goodEngine);
            car1.Start();

            Car car2 = new Car();
            IEngine badEngine = new BadEngine();
            car2.Inject(badEngine);
            car2.Start();
        }

    }


    public class Car : IDependOnEngine
    {
        private IEngine engine;

        public void Inject(IEngine engine)
        {
            this.engine = engine;
        }

        public void Start()
        {
            var startMessage = engine.StartMessage();
            Console.WriteLine(startMessage);
        }

    }
    public interface IDependOnEngine
    {
        void Inject(IEngine engine);
    }

    public interface IEngine
    {
        string StartMessage();

    }
    class GoodEngine : IEngine
    {
        public string StartMessage()
        {
            return "wrrrum wrrrumm";

        }
    }
    public class BadEngine : IEngine
    {
        public string StartMessage()
        {
            return "Nigdzie sie nie ruszam.";
        }
    }

}
