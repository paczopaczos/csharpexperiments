﻿using System;

namespace ManualDI
{
    public class Program
    {
        public void Main(string[] args)
        {
            IEngine goodEngine = new GoodEngine();
            IEngine badEngine = new BadEngine();

            Car car1 = new Car(goodEngine);
            Car car2 = new Car(badEngine);

            car1.Start();
            car2.Start();
            Console.Read();
        }
    }
    public class Car
    {
        private readonly IEngine engine;
        public Car(IEngine engine)
        {
            this.engine = engine;

        }
        public void Start()
        {
            var startMessage = engine.StartMessage();
            Console.WriteLine(startMessage);
        }

    }
    public interface IEngine
    {
         string StartMessage();
    }
    public class GoodEngine : IEngine
    {
        public string StartMessage()
        {
            return "wrrrum wrrrumm";
        }
    }

    public class BadEngine : IEngine
    {
        public string StartMessage()
        {
            return "Nigdzie sie nie ruszam.";
        }
    }
}
