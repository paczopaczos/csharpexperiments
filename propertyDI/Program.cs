﻿using System;

namespace propertyDI
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car1 = new Car();
            IEngine goodEngine = new GoodEngine();
            car1.Engine = goodEngine;
            car1.Start();

            Car car2 = new Car();
            IEngine badEngine = new BadEngine();
            car2.Engine = badEngine;
            car2.Start();
        }

    }


    public class Car
    {
        private IEngine engine;
        public IEngine Engine { set { engine = value; } }
        public void Start()
        {
            var startMessage = engine.StartMessage();
            Console.WriteLine(startMessage);
        }

    }
  


    public interface IEngine
    {
        string StartMessage();

    }
    class GoodEngine : IEngine
    {
        public string StartMessage()
        {
            return "wrrrum wrrrumm";

        }
    }
    public class BadEngine : IEngine
    {
        public string StartMessage()
        {
            return "Nigdzie sie nie ruszam.";
        }
    }

}
